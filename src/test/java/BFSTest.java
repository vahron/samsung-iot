import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertTrue;

public class BFSTest {
    @Test
    public void traverse_tree() {
        Map<Integer, Set<Integer>> tree = ImmutableMap.of(
                1, ImmutableSet.of(2, 3),
                2, ImmutableSet.of(),
                3, ImmutableSet.of(4, 5)
        );
        assertTrue(BFSTraverseUtil.traverse(1, tree)
                .equals(Lists.newArrayList(1, 2, 3, 4 ,5)));
    }

    @Test
    public void traverse_cycles() {
        Map<Integer, Set<Integer>> tree = ImmutableMap.of(
                1, ImmutableSet.of(1, 2),
                2, ImmutableSet.of(1, 3),
                3, ImmutableSet.of(3)
        );
        assertTrue(BFSTraverseUtil.traverse(1, tree)
                .equals(Lists.newArrayList(1, 2, 3)));
    }

    @Test
    public void traverse_bfs() {
        Map<Integer, Set<Integer>> tree = ImmutableMap.of(
                1, ImmutableSet.of(1, 2, 3, 4, 5),
                3, ImmutableSet.of(6)
        );
        assertTrue(BFSTraverseUtil.traverse(1, tree)
                .equals(Lists.newArrayList(1, 2, 3, 4, 5, 6)));

    }

    private static class BFSTraverseUtil {
        static List<Integer> traverse(int start, Map<Integer, Set<Integer>> graph) {
            List<Integer> traverse = new ArrayList<>();
            Set<Integer> visited = new HashSet<>();
            Deque<Integer> toVisit = new ArrayDeque<>();

            visited.add(start);
            toVisit.add(start);

            while (!toVisit.isEmpty()) {
                int node = toVisit.poll();
                traverse.add(node);

                Set<Integer> neighbours = graph.get(node);
                if (neighbours != null) {
                    for (Integer neighbour : neighbours) {
                        if (!visited.contains(neighbour)) {
                            visited.add(neighbour);
                            toVisit.add(neighbour);
                        }
                    }
                }
            }

            return traverse;
        }
    }
}
