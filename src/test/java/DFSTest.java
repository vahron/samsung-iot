import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertTrue;

public class DFSTest {
    @Test
    public void traverse_tree() {
        Map<Integer, Set<Integer>> tree = ImmutableMap.of(
                1, ImmutableSet.of(2, 3),
                2, ImmutableSet.of(),
                3, ImmutableSet.of(4, 5)
        );
        assertTrue(DFSTraverseUtil.traverse(1, tree)
                .equals(Lists.newArrayList(1, 3, 5, 4, 2)));
    }

    @Test
    public void traverse_cycles() {
        Map<Integer, Set<Integer>> tree = ImmutableMap.of(
                1, ImmutableSet.of(1, 2),
                2, ImmutableSet.of(1, 3),
                3, ImmutableSet.of(3)
        );
        assertTrue(DFSTraverseUtil.traverse(1, tree)
                .equals(Lists.newArrayList(1, 2, 3)));
    }

    @Test
    public void traverse_dfs() {
        Map<Integer, Set<Integer>> tree = ImmutableMap.of(
                1, ImmutableSet.of(1, 2, 3, 4, 5),
                3, ImmutableSet.of(6),
                6, ImmutableSet.of(7)
        );
        assertTrue(DFSTraverseUtil.traverse(1, tree)
                .equals(Lists.newArrayList(1, 5, 4, 3, 6, 7, 2)));

    }

    private static class DFSTraverseUtil {
        static List<Integer> traverse(int start, Map<Integer, Set<Integer>> graph) {
            List<Integer> traverse = new ArrayList<>();
            Set<Integer> visited = new HashSet<>();
            Deque<Integer> toVisit = new ArrayDeque<>();

            toVisit.push(start);

            while (!toVisit.isEmpty()) {
                int node = toVisit.pop();

                if (!visited.contains(node)) {
                    traverse.add(node);
                    visited.add(node);

                    Set<Integer> neighbours = graph.get(node);
                    if (neighbours != null) {
                        for (Integer neighbour : neighbours) {
                            toVisit.push(neighbour);
                        }
                    }
                }
            }

            return traverse;
        }
    }
}
